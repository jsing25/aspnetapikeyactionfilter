﻿# API Key Validation Filter Example

This is a basic example of how to use Action Filters to filter out requests.

## Usage

Keys can be passed as a query string in the request URL like so:

```
http://localhost:54736/api/protected?key=123
```

I've included a `TestController` that has a single protected method, and a `ProtectedController` where
all the methods are protected due to the `[ApiKeyActionFilter]` being placed over the controller.

## Additional Instructions

The main thing to pay attention to in this example is the `ApiKeyActionFilter.cs` file in the Middleware folder. 
That along with the attribute `[ApiKeyActionFilter]` are the only parts you'd need in your application. You'd also
need a way to generate the keys and retreive them from the database to check if they are valid, but that
shouldn't be too hard. Also, as I mentioned in one of the comments, just make sure the keys don't have any invalid
URL characters.

## Links

Here are a couple links that helped me create this example:

* https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/hands-on-labs/aspnet-mvc-4-custom-action-filters
* https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/filters?view=aspnetcore-2.0#authorization-filters