﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiKeyValidationFilterExample.Data
{
    public static class MockKeyDataStore
    {
        public static HashSet<string> GetValidKeys()
        {
            // In a real application, these would
            // be stored in your database, and be
            // much more secure. You could either
            // randomly generate them in your application
            // or have the database do it. Just make
            // sure whatever's generated is safe for
            // a URL.
            return new HashSet<string>
            {
                "abc",
                "xyz",
                "123"
            };
        }
    }
}
