﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiKeyValidationFilterExample.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ApiKeyValidationFilterExample.Middleware
{
    public class ApiKeyActionFilter : ActionFilterAttribute
    {
        // This method is called before the Action executes
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.Request.Query.TryGetValue("key", out var keyStringValues);
            var key = keyStringValues.FirstOrDefault();

            if (!ApiKeyValidator.IsValidKey(key))
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
