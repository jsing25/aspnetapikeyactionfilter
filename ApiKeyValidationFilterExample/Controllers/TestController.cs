﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiKeyValidationFilterExample.Middleware;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiKeyValidationFilterExample.Controllers
{
    public class TestController : Controller
    {
        [HttpGet]
        [Route("api/test")]
        public IActionResult Test()
        {
            return Ok("Test method works!");
        }

        [HttpGet]
        [Route("api/test/protected")]
        [ApiKeyActionFilter]
        public IActionResult ProtectedTest()
        {
            return Ok("The attrubute works on single methods too.");
        }
    }
}