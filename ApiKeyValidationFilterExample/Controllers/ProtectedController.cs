﻿using ApiKeyValidationFilterExample.Middleware;
using Microsoft.AspNetCore.Mvc;

namespace ApiKeyValidationFilterExample.Controllers
{
    [ApiKeyActionFilter]
    public class ProtectedController : Controller
    {
        [HttpGet]
        [Route("api/protected")]
        public IActionResult Test()
        {
            return Ok("You have a valid key!");
        }
    }
}