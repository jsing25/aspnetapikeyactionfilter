﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiKeyValidationFilterExample.Data;

namespace ApiKeyValidationFilterExample.Utilities
{
    public static class ApiKeyValidator
    {
        public static bool IsValidKey(string key)
        {
            return MockKeyDataStore
                .GetValidKeys()
                .Contains(key);
        }
    }
}
